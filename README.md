# Desafio Sólides

Criar um WebApp* para estagiários apontarem hora de trabalho. Este Webapp deve conter:

- Tela de login
- Tela de cadastro
- Controle de sessão via login
- Tela para apontar hora (chegada na empresa, hora de almoço, saída da empresa)
- Tela de histórico de apontamentos do usuário
- Design responsivo
- Um readme.txt explicando como fazer funcionar o seu sistema

*Webapp - Sistema web, que roda em um navegador, com design responsivo voltado para celulares.

Todo o desafio deve ser desenvolvido em React (utilize um backend à sua escolha para persistir os dados). Não se preocupe em fazer um super sistema lindo. O que será avaliado é como você estrutura seu código e seus conhecimentos de algoritmo. Os requisitos do sistema estão escrito de maneira bem rasa de propósito. É importante para gente ver como é sua linha de raciocínio e como você transforma informação em código ;)

## Instalação

O projeto foi desenvolvido utilizando React, Express e MongoDB. Foi criado containers dockers para auxiliar na execução do projeto.

Primeiro é necessário clonar o projeto:
```bash
git clone git@bitbucket.org:edusantosbrito/desafio-solides.git
cd ./desafio-solides
```

Para iniciar os containers em modo de produção (Estive com problemas com CORS no modo de produção, por conta de tempo não consegui resolvê-lo, favor executar somente em modo de Desenvolvimento), execute:
```bash
docker-compose up -f docker.compose.yml --build
```

Para iniciar os containers em modo de desenvolvimento, execute:
```bash
docker-compose up -f docker.compose.dev.yml --build
```

O projeto React irá rodar na porta 3000, o projeto Express irá rodar na porta 3001 e a instância do mongo irá rodar na porta 27017 
