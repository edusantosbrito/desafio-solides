import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import mongoose from 'mongoose';
import { HmacSHA1 } from 'crypto-js';
import jwt from 'jsonwebtoken';
import moment from 'moment';
import UserModel, { UserDTO } from './database/model/User';
import checkToken from './checkToken';
import ClockInModel, { ClockInDTO } from './database/model/ClockIn';

const {
    MONGODB_DATABASE,
    MONGODB_HOST,
    MONGODB_PORT,
    CRYPTO_KEY,
    TOKEN_SECRET,
} = process.env;

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(checkToken);

const mongoURI = `mongodb://${MONGODB_HOST}:${MONGODB_PORT}/${MONGODB_DATABASE}`;
mongoose.connect(mongoURI, { useNewUrlParser: true });

app.post(
    '/create-account',
    async (req, res) => {
        const userToSave: UserDTO = req.body;
        const userExist = await UserModel.findOne({ email: userToSave.email });
        if (userExist) {
            res.status(409).send({ message: 'Já existe um usuário com esse email' });
        } else {
            userToSave.password = HmacSHA1(userToSave.password, CRYPTO_KEY).toString();
            await UserModel.create(userToSave);
            res.status(200).send({ message: 'Usuário criado com sucesso!' });
        }
    },
);

app.post(
    '/auth',
    async (req, res) => {
        const userToLogin: { email: string; password: string } = req.body;
        const userExist = await UserModel.findOne({ email: userToLogin.email });
        if (userExist) {
            if (HmacSHA1(userToLogin.password, CRYPTO_KEY).toString() === userExist.password) {
                const token = jwt.sign({ email: userExist.email }, TOKEN_SECRET, { expiresIn: '24h' });
                res.status(200).send({ message: `Seja bem vindo ${userExist.name}!`, token });
            } else {
                res.status(401).send({ message: 'Senha incorreta, tente novamente.' });
            }
        } else {
            res.status(404).send({ message: 'Não foi encontrado nenhum usuário com esse email' });
        }
    },
);

app.post(
    '/clock-in',
    async (req, res) => {
        const clockInData: {
            userId: string;
            selectedTime: Date;
        } = req.body;
        const clockIns = await ClockInModel.find({
            userId: mongoose.Types.ObjectId(clockInData.userId),
            createOn: {
                $gte: moment().startOf('day'),
                $lt: moment().endOf('day'),
            },
        });
        const newClockIn: ClockInDTO = {
            userId: clockInData.userId,
            createOn: clockInData.selectedTime.toString(),
            type: '',
        };
        if (clockIns.length === 0) {
            newClockIn.type = 'start';
        } else if (clockIns.length === 1) {
            newClockIn.type = 'lunchStart';
        } else if (clockIns.length === 2) {
            newClockIn.type = 'lunchEnd';
        } else if (clockIns.length === 3) {
            newClockIn.type = 'end';
        } else {
            res.status(405).send({ message: 'Você já bateu todos os seus pontos de hoje' });
        }
        await ClockInModel.create({
            userId: mongoose.Types.ObjectId(clockInData.userId),
            createOn: moment(newClockIn.createOn).toDate(),
            type: newClockIn.type,
        });
        res.status(200).send({ message: 'Ponto batido com sucesso!' });
    },
);

app.get(
    '/clock-ins/:userId',
    async (req, res) => {
        const {
            userId,
        } = req.params;
        if (userId) {
            const clockIns = (await ClockInModel.find({ userId: mongoose.Types.ObjectId(userId) }));
            res.status(200).send(
                clockIns.sort(
                    (a, b) => (moment(a.createOn).unix() > moment(b.createOn).unix() ? -1 : 1),
                ),
            );
        } else {
            res.status(400).send({ message: 'Parâmetros insuficientes' });
        }
    },
);

app.get(
    '/user/:token',
    async (req, res) => {
        const {
            token,
        } = req.params;
        await jwt.verify(
            token,
            TOKEN_SECRET,
            async (err, decoded: { email: string; iat: number; exp: number }) => {
                res.status(200).send(await UserModel.findOne({ email: decoded.email }));
            },
        );
    },
);

app.listen(process.env.NODE_PORT || 3000);
