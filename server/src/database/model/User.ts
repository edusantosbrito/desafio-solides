import { Typegoose, prop } from '@hasezoey/typegoose';

export class User extends Typegoose {
    @prop({ required: true })
    name!: string;

    @prop({ required: true })
    email!: string;

    @prop({ required: true })
    password!: string;
}

export interface UserDTO {
    name: string;
    email: string;
    password: string;
}

const UserModel = new User().getModelForClass(User);

export default UserModel;
