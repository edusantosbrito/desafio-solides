import { Typegoose, prop } from '@hasezoey/typegoose';
import { Types } from 'mongoose';

export class ClockIn extends Typegoose {
    @prop({ required: true })
    userId!: Types.ObjectId;

    @prop({ required: true })
    createOn!: Date;

    @prop({ required: true })
    type!: string;
}

export interface ClockInDTO {
    userId: string;
    createOn: string;
    type: string;
}

const ClockInModel = new ClockIn().getModelForClass(ClockIn);

export default ClockInModel;
