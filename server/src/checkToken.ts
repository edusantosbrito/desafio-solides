import jwt, { VerifyErrors } from 'jsonwebtoken';
import express, { NextFunction } from 'express';

const checkToken = (
    req: express.Request,
    res: express.Response,
    next: NextFunction,
): any => {
    if (
        req.header('referer').includes('/login')
        || req.header('referer').includes('/create-account')
    ) {
        return next();
    }
    let token = req.header('Authorization');
    if (token) {
        if (token.startsWith('Bearer ')) {
            token = token.slice(7, token.length);
        }
        jwt.verify(
            token,
            process.env.TOKEN_SECRET,
            (err: VerifyErrors) => {
                if (err) {
                    return res.status(401).send({
                        message: 'Não autorizado',
                    });
                }
                return next();
            },
        );
    } else {
        return res.status(401).send({
            message: 'Não autorizado',
        });
    }
    return null;
};

export default checkToken;
