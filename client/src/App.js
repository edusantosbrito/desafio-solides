import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import CreateAccount from "./components/CreateAccount";
import Dashboard from "./components/Dashboard";
import Login from "./components/Login";
import NotifyProvider from "./context/NotifyProvider";
import UserProvider from "./context/UserProvider";
import History from "./components/History";

function App() {
  return (
    <Router>
      <UserProvider>
        <NotifyProvider>
          <Route exact path="/" component={Dashboard} />
          <Route exate path="/login" component={Login} />
          <Route exate path="/criar-conta" component={CreateAccount} />
          <Route exate path="/historico" component={History} />
        </NotifyProvider>
      </UserProvider>
    </Router>
  );
}

export default App;
