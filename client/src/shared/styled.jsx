import { TextField, Paper, Button, AppBar } from "@material-ui/core";
import styled, { css } from "styled-components";

const theme = {
  color: {
    main: "#000066",
    dark: "#001247",
    light: "#809FFF",
    lighter: "#BFCFFF",
    contrast: "#FFFFFF"
  },
  spacing: 4
};

export const DefaultInput = styled(TextField)`
  && {
    border-color: ${theme.color.main};
    ${({ marginSpacing }) =>
    marginSpacing &&
    css`
        margin: ${marginSpacing * theme.spacing}px;
      `}
  }
`;

export const DefaultButton = styled(Button)`
  && {
    width: 120px;
    background-color: ${theme.color.main};
    color: ${theme.color.contrast};
    :hover {
      background-color: ${theme.color.dark};
    }

  }
`;

export const FlexContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  ${({ height }) =>
    height &&
    css`
      height: ${height};
    `}

  ${({ width }) =>
    width &&
    css`
      width: ${width};
    `}

  ${({ justifyContent }) =>
    justifyContent &&
    css`
      justify-content: ${justifyContent};
    `}

  ${({ alignItems }) =>
    alignItems &&
    css`
      align-items: ${alignItems};
    `}

  ${({ flexDirection }) =>
    flexDirection &&
    css`
      flex-direction: ${flexDirection};
    `}

  ${({ backgroundImage }) =>
    backgroundImage &&
    css`
      background-image: url(${backgroundImage});
      background-size: cover;
      background-position: center center;
      background-repeat: no-repeat;
    `}

`;

export const PaperContainer = styled(Paper)`
  && {
    display: flex;
    justify-content: center;
    align-items: center;
    padding: ${theme.spacing * 4}px;

    ${({ height }) =>
    height &&
    css`
        height: ${height};
      `}

    ${({ width }) =>
    width &&
    css`
        width: ${width};
      `}

    ${({ justifyContent }) =>
    justifyContent &&
    css`
        justify-content: ${justifyContent};
      `}

    ${({ alignItems }) =>
    alignItems &&
    css`
        align-items: ${alignItems};
      `}

    ${({ flexDirection }) =>
    flexDirection &&
    css`
        flex-direction: ${flexDirection};
      `}
  };
`;

export const AppDrawerBar = styled(AppBar)`
  && {
    background-color: ${theme.color.main};
  }
`
