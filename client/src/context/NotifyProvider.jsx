import React, { useMemo, createContext, useState, useContext } from "react";
import { SnackbarContent, IconButton, Snackbar } from "@material-ui/core";
import { amber, green, blue, red } from "@material-ui/core/colors";
import { Check, Error, Info, Warning, Close } from "@material-ui/icons";
import { makeStyles } from "@material-ui/styles";
import clsx from "clsx";

const useStyles = makeStyles(theme => ({
  success: {
    backgroundColor: green[600]
  },
  error: {
    backgroundColor: red[600]
  },
  info: {
    backgroundColor: blue[500]
  },
  warning: {
    backgroundColor: amber[700]
  },
  icon: {
    fontSize: 20
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: 8
  },
  message: {
    display: "flex",
    alignItems: "center"
  }
}));

function SnackbarContentWrapper(props) {
  const classes = useStyles();
  const variantIcon = useMemo(
    () => ({
      success: Check,
      warning: Warning,
      error: Error,
      info: Info
    }),
    []
  );
  const { className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={clsx(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={clsx(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="close"
          color="inherit"
          onClick={onClose}
        >
          <Close className={classes.icon} />
        </IconButton>
      ]}
      {...other}
    />
  );
}

const NotifyContext = createContext({
  open: false,
  notify: ({ message, variant }) => {}
});

export function useNotify() {
  return useContext(NotifyContext);
}
const NotifyProvider = ({ children }) => {
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");
  const [variant, setVariant] = useState("");

  function notify(params) {
    setMessage(params.message);
    setVariant(params.variant);
    setOpen(true);
  }
  function handleClose(event, reason) {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  }
  return (
    <NotifyContext.Provider value={{ open, notify }}>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right"
        }}
        open={open}
        autoHideDuration={2000}
        onClose={handleClose}
      >
        <SnackbarContentWrapper
          onClose={handleClose}
          variant={variant}
          message={message}
        />
      </Snackbar>
      {children}
    </NotifyContext.Provider>
  );
};

export default NotifyProvider;
