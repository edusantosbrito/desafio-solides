import React, { createContext, useContext, useState, useEffect, useCallback } from 'react'
import useRouter from 'use-react-router';

const UserContext = createContext({
    user: null,
    token: null,
    changeUserData: () => { },
    changeToken: () => { }
})

export const useUserContext = () => useContext(UserContext);

const UserProvider = ({ children }) => {
    const [user, setUser] = useState(null)
    const [token, setToken] = useState(null)
    const { history } = useRouter();

    const fetchUser = useCallback(() => {
        const headers = new Headers();
        headers.append('Authorization', token);
        fetch(`http://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}/user/${token}`, {
            headers
        })
            .then(response => response.json())
            .then(data => setUser(data));
    }, [token])

    useEffect(() => {
        if (!token && !localStorage.getItem('token')) {
            return history.push('/login');
        }
        if (!token) {
            return setToken(localStorage.getItem('token'));
        }
        if (!user) {
            return fetchUser(token);
        }
    }, [fetchUser, history, history.push, token, user])

    function changeUserData(data) {
        setUser(data);
    }

    function changeToken(token) {
        setToken(token);
    }

    return <UserContext.Provider value={{ user, token, changeUserData, changeToken }}>
        {children}
    </UserContext.Provider>

}
export default UserProvider
