import { Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';
import React, { useMemo } from 'react';
import useClockIns from '../../hooks/useClockIns';
import { PaperContainer } from '../../shared/styled';
import AppDrawer from '../../shared/AppDrawer';
import moment from 'moment';

const History = () => {
    const { clockIns, loading } = useClockIns();

    const CLOCK_IN_TYPE = useMemo(() => ({
        start: 'Entrada',
        lunchStart: 'Início do horário do almoço',
        lunchEnd: 'Fim do horário do almoço',
        end: 'Saída'
    }), [])

    return !loading && (
        <AppDrawer title={'Histórico'}>
            <PaperContainer>
                <Table>
                    <TableHead>
                        <TableCell>Data</TableCell>
                        <TableCell>Tipo</TableCell>
                    </TableHead>
                    <TableBody>
                        {clockIns.map(clockIn => {
                            return <TableRow>
                                <TableCell>{moment(clockIn.createOn).format("DD/MM/YYYY HH:mm:ss")}</TableCell>
                                <TableCell>{CLOCK_IN_TYPE[clockIn.type]}</TableCell>
                            </TableRow>
                        })}
                    </TableBody>
                </Table>
            </PaperContainer>
        </AppDrawer>
    )
}

export default History
