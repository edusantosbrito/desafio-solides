import { Typography, CircularProgress } from "@material-ui/core";
import React, { useState, useCallback } from "react";
import { Link } from "react-router-dom";
import { useNotify } from "../../context/NotifyProvider";
import useRouter from 'use-react-router';
import {
  DefaultButton,
  DefaultInput,
  FlexContainer,
  PaperContainer
} from "../../shared/styled";
import LoginImage from "../../static/login.jpg";
import LogoSolides from "../../static/logoSolides.svg";
import { validateEmail } from "../../utils";
import { useUserContext } from "../../context/UserProvider";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const { history } = useRouter();
  const { notify } = useNotify();
  const { changeToken } = useUserContext();

  const sendToServer = useCallback(async () => {
    return fetch(
      `http://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}/auth`,
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({
          email,
          password
        })
      }
    );
  }, [email, password]);

  const validate = useCallback(async () => {
    setLoading(true);
    try {
      if (!email.trim() || !password.trim()) {
        throw new Error("Por favor, preencha todos os campos para continuar");
      }
      if (!validateEmail(email)) {
        throw new Error("Email inválido, favor corrigir e tentar novamente");
      }
      sendToServer()
        .then(async (response) => {
          if (response.ok) {
            const data = await response.json();
            notify({ message: data.message, variant: "success" });
            changeToken(data.token);
            localStorage.setItem("token", data.token);
            setTimeout(() => history.push('/'), 2000)
            setLoading(false);
          } else {
            throw new Error((await response.json()).message);
          }
        })
        .catch(error => {
          notify({ message: error.message, variant: "error" });
          setLoading(false);
        });
    } catch (error) {
      notify({ message: error.message, variant: "error" });
      setLoading(false);
    }
  }, [changeToken, email, history, notify, password, sendToServer])

  return (
    <FlexContainer width="100%" height="100%" backgroundImage={LoginImage}>
      <PaperContainer
        justifyContent="space-evenly"
        flexDirection="column"
        height="400px"
      >
        <img
          src={LogoSolides}
          alt="Logotipo da Sólides, com Sólides escrito à esquerda e 4 linhas horizontais coloridas de diferentes tamanhos na direita"
        />
        <DefaultInput
          type="email"
          label="Email"
          value={email}
          onChange={event => setEmail(event.target.value)}
          variant="outlined"
          autoComplete="email"
        />
        <DefaultInput
          type="password"
          label="Senha"
          value={password}
          onChange={event => setPassword(event.target.value)}
          variant="outlined"
          autoComplete="password"
        />
        <FlexContainer flexDirection="column">
          <DefaultButton
            onClick={validate}
          >
            {loading ? (
              <CircularProgress color="#ffffff" size="24px" />
            ) : (
                "Entrar"
              )}
          </DefaultButton>
          <Typography variant="caption">Não tem uma conta?</Typography>
          <Link to="/criar-conta">
            <Typography variant="caption">Criar conta</Typography>
          </Link>
        </FlexContainer>
      </PaperContainer>
    </FlexContainer>
  );
};

export default Login;
