import { Typography, CircularProgress } from "@material-ui/core";
import React, { useState, useCallback } from "react";
import { Link } from "react-router-dom";
import { useNotify } from "../../context/NotifyProvider";
import {
  DefaultButton,
  DefaultInput,
  FlexContainer,
  PaperContainer
} from "../../shared/styled";
import LoginImage from "../../static/login.jpg";
import LogoSolides from "../../static/logoSolides.svg";
import { validateEmail } from "../../utils";
import useRouter from "use-react-router";

const CreateAccount = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const { notify } = useNotify();
  const { history } = useRouter();

  const sendToServer = useCallback(async () => {
    return fetch(
      `http://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}/create-account`,
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({
          name,
          email,
          password
        })
      }
    );
  }, [email, name, password]);

  const validate = useCallback(() => {
    setLoading(true);
    try {
      if (
        !name.trim() ||
        !email.trim() ||
        !password.trim() ||
        !confirmPassword.trim()
      ) {
        throw new Error("Por favor, preencha todos os campos para continuar");
      }
      if (!validateEmail(email)) {
        throw new Error("Email inválido, favor corrigir e tentar novamente");
      }
      if (password.length < 8) {
        throw new Error("Por favor, digite uma senha com no mínimo 8 dígitos");
      }
      if (password !== confirmPassword) {
        throw new Error(
          "Senhas não se coincidem, favor corrigir e tentar novamente"
        );
      }
      sendToServer()
        .then(async (response) => {
          if (response.ok) {
            notify({ message: (await response.json()).message, variant: "success" });
            setTimeout(() => { history.push('/login') }, 2000)
            setLoading(false);
          } else {
            throw new Error((await response.json()).message);
          }
        })
        .catch(error => {
          notify({ message: error.message, variant: "error" });
          setLoading(false);
        });
    } catch (error) {
      notify({ message: error.message, variant: "error" });
      setLoading(false);
    }
  }, [confirmPassword, email, history, name, notify, password, sendToServer]);

  return (
    <FlexContainer width="100%" height="100%" backgroundImage={LoginImage}>
      <PaperContainer
        justifyContent="space-evenly"
        flexDirection="column"
        height="400px"
      >
        <img
          src={LogoSolides}
          alt="Logotipo da Sólides, com Sólides escrito à esquerda e 4 linhas horizontais coloridas de diferentes tamanhos na direita"
        />
        <FlexContainer flexDirection="column">
          <FlexContainer>
            <DefaultInput
              marginSpacing={1}
              type="text"
              label="Nome Completo"
              value={name}
              onChange={event => setName(event.target.value)}
              variant="outlined"
              autoComplete="name"
            />
            <DefaultInput
              marginSpacing={1}
              type="email"
              label="Email"
              value={email}
              onChange={event => setEmail(event.target.value)}
              variant="outlined"
              autoComplete="email"
            />
          </FlexContainer>
          <FlexContainer>
            <DefaultInput
              marginSpacing={1}
              type="password"
              label="Senha"
              value={password}
              onChange={event => setPassword(event.target.value)}
              variant="outlined"
              autoComplete="new-password"
            />
            <DefaultInput
              marginSpacing={1}
              type="password"
              label="Confirmar Senha"
              value={confirmPassword}
              onChange={event => setConfirmPassword(event.target.value)}
              variant="outlined"
              autoComplete="new-password"
            />
          </FlexContainer>
        </FlexContainer>
        <FlexContainer flexDirection="column">
          <DefaultButton onClick={validate}>
            {loading ? (
              <CircularProgress color="#ffffff" size="24px" />
            ) : (
                "Cadastrar"
              )}
          </DefaultButton>
          <Typography variant="caption">Já tem uma conta?</Typography>
          <Link to="/login">
            <Typography variant="caption">Ir para o Login</Typography>
          </Link>
        </FlexContainer>
      </PaperContainer>
    </FlexContainer>
  );
};

export default CreateAccount;
