import { Typography, CircularProgress } from "@material-ui/core";
import React, { useState, useEffect, useCallback } from "react";
import moment from 'moment-timezone';
import { useUserContext } from "../../context/UserProvider";
import AppDrawer from "../../shared/AppDrawer";
import { FlexContainer, DefaultButton } from "../../shared/styled";
import { useNotify } from "../../context/NotifyProvider";

const Dashboard = () => {
  const { user, token } = useUserContext();
  const [actualTime, setActualTime] = useState(moment().format('DD/MM/YYYY HH:mm:ss'))
  const [loading, setLoading] = useState(false);
  const { notify } = useNotify();

  useEffect(() => {
    setInterval(() => setActualTime(moment().format('DD/MM/YYYY HH:mm:ss')), 1000)
  }, [actualTime])

  const sendToServer = useCallback((selectedTime) => {
    if (user && selectedTime) {
      const { _id } = user;
      setLoading(true);
      const headers = new Headers();
      headers.append('Authorization', token);
      headers.append('Content-type', 'application/json')
      fetch(
        `http://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}/clock-in`,
        {
          method: 'POST',
          body: JSON.stringify({ userId: _id, selectedTime: moment(selectedTime, 'DD/MM/YYYY HH:mm:ss').tz('America/Sao_Paulo').toDate() }),
          headers
        }
      )
        .then(async (response) => {
          if (response.ok) {
            const data = await response.json();
            notify({ message: data.message, variant: "success" })
            setLoading(false);
          } else {
            throw new Error((await response.json()).message);
          }
        })
        .catch(error => {
          notify({ message: error.message, variant: "error" });
          setLoading(false);
        });
    }
  }, [notify, token, user])

  return <AppDrawer
    title={'Dashboard'}
  >
    {user && <div>
      <FlexContainer flexDirection='column'>
        <Typography variant='h6'>Bem-vindo {user.name}!</Typography>
        <FlexContainer flexDirection='column'>
          <Typography variant='body1'>{actualTime}</Typography>
          <DefaultButton onClick={() => {
            sendToServer(actualTime)
          }}>{loading ? (
            <CircularProgress color="#ffffff" size="24px" />
          ) : (
              "Bater Ponto"
            )}</DefaultButton>
        </FlexContainer>
      </FlexContainer>
    </div>}

  </AppDrawer>;
};

export default Dashboard;
