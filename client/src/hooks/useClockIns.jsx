import { useCallback, useEffect, useState } from 'react';
import { useNotify } from '../context/NotifyProvider';
import { useUserContext } from '../context/UserProvider';

const useClockIns = () => {
    const [clockIns, setClockIns] = useState([]);
    const { user, token } = useUserContext();
    const [loading, setLoading] = useState(false);
    const { notify } = useNotify();

    const fetchData = useCallback(async () => {
        if (user && clockIns.length === 0) {
            setLoading(true);
            const headers = new Headers();
            headers.append('Authorization', token);
            headers.append('Content-type', 'application/json')
            fetch(
                `http://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}/clock-ins/${user._id}`,
                {
                    headers
                }
            ).then(async (response) => {
                if (response.ok) {
                    const data = await response.json();
                    setClockIns(data);
                    setLoading(false);
                } else {
                    throw new Error((await response.json()).message);
                }
            })
                .catch(error => {
                    notify({ message: error.message, variant: "error" });
                    setLoading(false);
                });
        }
    }, [clockIns.length, notify, token, user])

    useEffect(() => {
        if (user) {
            fetchData()
        }
    }, [fetchData, user])

    return { clockIns, loading }
}

export default useClockIns
